import Vue from 'vue';
import Vuex, { MutationTree, ActionTree } from 'vuex';
import { User } from '@/models/user';

Vue.use(Vuex);

interface State {
    users: User[];
    offsetUsers: User[];
}

const getters = {
    users(users: State) {
        return users.users;
    },
    offsetUsers(offsetUsers: State) {
        return offsetUsers.offsetUsers;
    },
};

const state: {} = {
    offsetUsers : [
        {
            id: 1,
            firstname: 'Fatma',
            lastname: 'Al-wazni',
            active: true,
        },
        {
            id: 2,
            firstname: 'Ellen',
            lastname: 'Borg',
            active: true,
        },
        {
            id: 3,
            firstname: 'Laura',
            lastname: 'Camenzind',
            active: true,
        },
        {
            id: 4,
            firstname: 'Lukas',
            lastname: 'Diebold',
            active: true,
        },
        {
            id: 5,
            firstname: 'Gian',
            lastname: 'Dogwiler',
            active: true,
        },
        {
            id: 6,
            firstname: 'Sumeyra',
            lastname: 'Duran',
            active: true,
        },
        {
            id: 7,
            firstname: 'Sophia',
            lastname: 'Eichler',
            active: true,
        },
        {
            id: 8,
            firstname: 'Zelia',
            lastname: 'Färber',
            active: true,
        },
        {
            id: 9,
            firstname: 'Felix',
            lastname: 'Hegg',
            active: true,
        },
        {
            id: 10,
            firstname: 'Daniel',
            lastname: 'Hunziker',
            active: true,
        },
        {
            id: 11,
            firstname: 'Aline',
            lastname: 'Jonscher',
            active: true,
        },
        {
            id: 12,
            firstname: 'Pasang',
            lastname: 'Kangchen',
            active: true,
        },
        {
            id: 13,
            firstname: 'Colin',
            lastname: 'Mazenauer',
            active: true,
        },
        {
            id: 14,
            firstname: 'Alessandro',
            lastname: 'Parolini',
            active: true,
        },
        {
            id: 15,
            firstname: 'Justin',
            lastname: 'Kieninger',
            active: false,
        },
        {
            id: 16,
            firstname: 'Mark',
            lastname: 'Marolf',
            active: false,
        },
        {
            id: 17,
            firstname: 'Sara',
            lastname: 'Popovic',
            active: true,
        },
        {
            id: 18,
            firstname: 'Arian',
            lastname: 'Tahirukaj',
            active: true,
        },
        {
            id: 19,
            firstname: 'Alejandro',
            lastname: 'Van Engelen',
            active: true,
        },
    ],
    users : [
        {
            id: 1,
            firstname: 'Fatma',
            lastname: 'Al-wazni',
            active: true,
        },
        {
            id: 2,
            firstname: 'Ellen',
            lastname: 'Borg',
            active: true,
        },
        {
            id: 3,
            firstname: 'Laura',
            lastname: 'Camenzind',
            active: true,
        },
        {
            id: 4,
            firstname: 'Lukas',
            lastname: 'Diebold',
            active: true,
        },
        {
            id: 5,
            firstname: 'Gian',
            lastname: 'Dogwiler',
            active: true,
        },
        {
            id: 6,
            firstname: 'Sumeyra',
            lastname: 'Duran',
            active: true,
        },
        {
            id: 7,
            firstname: 'Sophia',
            lastname: 'Eichler',
            active: true,
        },
        {
            id: 8,
            firstname: 'Zelia',
            lastname: 'Färber',
            active: true,
        },
        {
            id: 9,
            firstname: 'Felix',
            lastname: 'Hegg',
            active: true,
        },
        {
            id: 10,
            firstname: 'Daniel',
            lastname: 'Hunziker',
            active: true,
        },
        {
            id: 11,
            firstname: 'Aline',
            lastname: 'Jonscher',
            active: true,
        },
        {
            id: 12,
            firstname: 'Pasang',
            lastname: 'Kangchen',
            active: true,
        },
        {
            id: 13,
            firstname: 'Colin',
            lastname: 'Mazenauer',
            active: true,
        },
        {
            id: 14,
            firstname: 'Alessandro',
            lastname: 'Parolini',
            active: true,
        },
        {
            id: 15,
            firstname: 'Justin',
            lastname: 'Kieninger',
            active: false,
        },
        {
            id: 16,
            firstname: 'Mark',
            lastname: 'Marolf',
            active: false,
        },
        {
            id: 17,
            firstname: 'Sara',
            lastname: 'Popovic',
            active: true,
        },
        {
            id: 18,
            firstname: 'Arian',
            lastname: 'Tahirukaj',
            active: true,
        },
        {
            id: 19,
            firstname: 'Alejandro',
            lastname: 'Van Engelen',
            active: true,
        },
    ],
};

const module = {
    namespaced: true,
    state,
    getters,
};

export default module;
