import { User } from '@/models/user';

export interface Week {
    id: number;
    startdate: string;
    enddate: string;
    vacation: boolean;
    user1: string;
    user2: string;
}
